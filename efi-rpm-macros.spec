%global debug_package %{nil}
%global _efi_vendor_ %(eval sed -n -e 's/^ID=//p' /etc/os-info)
%global _efi_vendor_dir %(eval sed -n -e 's/^ID=//p' /etc/os-info | awk -F '"' '{print $2}')

Name:          efi-rpm-macros
Version:       4
Release:       3
Summary:       A set of EFI-related RPM Macros
License:       GPLv3+
URL:           https://github.com/rhboot/%{name}/
Source0:       https://github.com/rhboot/%{name}/releases/download/%{version}/%{name}-%{version}.tar.bz2
Patch9000:     modify-os-release-to-os-info.patch
 
#patch backport from upstream community
BuildRequires: git sed
BuildArch:     noarch

%description
Efi-rpm-macros help to build EFI-Related packages with a set of common RPM Macros.

%package -n efi-srpm-macros
Summary: A set of EFI-related SRPM Macros
BuildArch: noarch
Requires: rpm

%description -n efi-srpm-macros
Efi-srpm-macros help to build EFI-related packages with a set of common SRPM macros.

%package -n efi-filesystem
Summary: Basic diretory layout of EFI machines
BuildArch: noarch
Requires: filesystem

%description -n efi-filesystem
Efi-filesystem is the structure and logic rules used to manage the groups of
information of EIF machines, it contains the basic directory layout for EFI
machine bootloaders and tools.

%prep
%autosetup -S git -n %{name}-4 -p1
git config --local --add efi.vendor "%{_vendor}"
git config --local --add efi.esp-root /boot/efi
git config --local --add efi.arches "x86_64 aarch64"

%build
%make_build EFI_VENDOR=%{_efi_vendor_} clean all

%install
%make_install EFI_VENDOR=%{_efi_vendor_}

%files -n efi-srpm-macros
%license LICENSE
%doc README
%{_rpmmacrodir}/macros.efi-srpm

%files -n efi-filesystem
%defattr(0700,root,root,-)
%dir /boot/efi/EFI/BOOT
%dir /boot/efi/EFI/%{_efi_vendor_dir}
%{_prefix}/lib/rpm/brp-boot-efi-times

%changelog
* Thu May 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 4-3
- modify os-release to os-info

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 4-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:bugfix in build process

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 4-1
- Package update. 

* Tue Sep 17 2019 yanzhihua <yanzhihua4@huawei.com> - 3-4
- Package init.

